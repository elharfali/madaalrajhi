<?php
namespace Godogi\MadaAlrajhi\Model;

class MadaAlrajhiConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    public function getConfig()
    {
        return [
            'key' => 'madaalrajhi'
        ];
    }
}
